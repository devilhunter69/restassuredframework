package testscripts;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import actions.CreateBookingActions;
import models.BookingDates;
import payload.CreateBookingRequest;
import payload.CreateBookingResponse;
import utilities.ExcelUtils;
import utilities.ExtentReportUtils;

public class CreateBookingTests extends BaseTest {
	
	
	

	
	@DataProvider
	public Object[][] getDataForCreateBooking() throws EncryptedDocumentException, IOException{
		ExcelUtils ex = new ExcelUtils("PayloadData.xlsx");
		return ex.getDataForDataProvider("payload1");
	}
	
	
	
	@Test(dataProvider = "getDataForCreateBooking")
	public void createBooking(String firstName, String lastName, String totalPrice, String depositePaid, String checkin, String checkout, String additionalNeeds) {
		
		ExtentTest test = extentReportUtils.getExtentReports().createTest("Create booking with proper data");
		
		//Create payload
		CreateBookingRequest createBookingRequest  = new CreateBookingRequest();
		BookingDates bookingdates = new BookingDates();
		bookingdates.setCheckin(checkin);
		bookingdates.setCheckout(checkout);
	
		
		createBookingRequest.setFirstname(firstName);
		createBookingRequest.setLastname(lastName);
		createBookingRequest.setTotalprice(Integer.parseInt(totalPrice));
		createBookingRequest.setDepositpaid(Boolean.parseBoolean(depositePaid));
		createBookingRequest.setBookingDates(bookingdates);
		createBookingRequest.setAdditionalneeds(additionalNeeds);
		
		CreateBookingActions createBookingActions = new CreateBookingActions(test);
		
		CreateBookingResponse createBookingResponse =  createBookingActions.createBooking(createBookingRequest);
	
		createBookingActions.verifyStatusCodeAs(200);
		createBookingActions.verifyBookingIdGenerated();
		
	}

}
