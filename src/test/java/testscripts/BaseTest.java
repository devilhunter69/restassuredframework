package testscripts;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import utilities.ExtentReportUtils;

public class BaseTest {
	
	static ExtentReportUtils extentReportUtils;
	
	@BeforeSuite
	public void setUp() {
		 extentReportUtils = new ExtentReportUtils("Restful Booker Api Test Report");	
	}
	
	@AfterSuite
	public void tearDown() {
		extentReportUtils.generateExtentReport();
	}

}
