package testscripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import actions.GenerateAuthActions;
import payload.GenerateAuthRequest;
import payload.GenerateAuthResponse;

public class GenerateAuthTest extends BaseTest {
	
	@Test
	public void testGenerateAuth() {
		
		ExtentTest test = extentReportUtils.getExtentReports().createTest("generate token test");
		
		
		GenerateAuthRequest generateAuthRequest = new GenerateAuthRequest();
		generateAuthRequest.setUsername("admin");
		generateAuthRequest.setPassword("password123");
		
		GenerateAuthActions generateAuthActions = new GenerateAuthActions(test);
		
		GenerateAuthResponse generateAuthResponse = generateAuthActions.generateAuth(generateAuthRequest);
		generateAuthActions.verifyStatusCodeAs(200);
		generateAuthActions.verifyTokenGenerated();
		

		
		
	}

}
