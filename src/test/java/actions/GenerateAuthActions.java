package actions;

import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;

import DataStore.Data;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import payload.CreateBookingRequest;
import payload.CreateBookingResponse;
import payload.GenerateAuthRequest;
import payload.GenerateAuthResponse;

public class GenerateAuthActions {
	
	Response response;
	GenerateAuthResponse generateAuthResponse;
	ExtentTest test;
	
	public GenerateAuthActions(ExtentTest test) {
		this.test = test;
	}
	
public GenerateAuthResponse generateAuth(GenerateAuthRequest generateAuthRequest) {
		
		RestAssured.baseURI = "https://restful-booker.herokuapp.com/";
		RestAssured.basePath = "auth";
		
		test.info("Base URI is: "+ Data.BASE_URL);
		test.info("Base path is: "+ RestAssured.basePath);
		
		test.info("Payload:" +generateAuthRequest);
		
		
		
		response = RestAssured
			.given()
			.body(generateAuthRequest)
			.contentType(ContentType.JSON)
			.log()
			.all()
			.post();
		test.info("Status code is: "+response.getStatusCode());
		
		generateAuthResponse = response.as(GenerateAuthResponse.class);
		return generateAuthResponse;
		
		
	}
	
	public void verifyStatusCodeAs(int expectedStatusCode) {
		
		int actualCode = response.getStatusCode();
		boolean flag = expectedStatusCode == actualCode;
		if(flag)
			test.pass("Status code is as expected: "+expectedStatusCode);
		else 
			test.fail("Status code is not as expected: "+expectedStatusCode);
	}
	
	public void verifyTokenGenerated() {
		String token = generateAuthResponse.getToken();
		System.out.println(token);
		Assert.assertEquals(token, token);;
	}
	
	
	
	

}
