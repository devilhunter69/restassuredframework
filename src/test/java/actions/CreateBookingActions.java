package actions;

import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;

import DataStore.Data;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import payload.CreateBookingRequest;
import payload.CreateBookingResponse;



public class CreateBookingActions {
	Response response;
	CreateBookingResponse createBookingResponse;
	ExtentTest test;
	
	public CreateBookingActions(ExtentTest test) {
		this.test = test;
	}
	
	
	public CreateBookingResponse createBooking(CreateBookingRequest createBookingRequest) {
		
		RestAssured.baseURI = Data.BASE_URL;
		RestAssured.basePath = Data.CREATE_BOOKING_BASEPATH;
		
		test.info("Base URI is: "+ Data.BASE_URL);
		test.info("Base path is: "+ RestAssured.basePath);
		
		test.info("Payload:" +createBookingRequest);
		
		
		
		response = RestAssured
			.given()
			.body(createBookingRequest)
			.contentType(ContentType.JSON)
			.log()
			.all()
			.post();
		test.info("Status code is: "+response.getStatusCode());
		
		createBookingResponse = response.as(CreateBookingResponse.class);
		return createBookingResponse;
		
		
	}
	
	public void verifyStatusCodeAs(int expectedStatusCode) {
		
		int actualCode = response.getStatusCode();
		boolean flag = expectedStatusCode == actualCode;
		if(flag)
			test.pass("Status code is as expected: "+expectedStatusCode);
		else 
			test.fail("Status code is not as expected: "+expectedStatusCode);
	}
	
	public void verifyBookingIdGenerated() {
		int bookingId =  createBookingResponse.getBookingid();
		Assert.assertTrue(bookingId>0, "No valid booking id");
	}
		
	

}
