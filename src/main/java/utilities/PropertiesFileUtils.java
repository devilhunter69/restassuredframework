package utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.testng.Assert;

public class PropertiesFileUtils {

	private Properties properties;

	public PropertiesFileUtils(String fileName) throws IOException {

		if (properties == null) {

			InputStream fis = PropertiesFileUtils.class.getClassLoader()
					.getResourceAsStream("propertiesFiles/" + fileName + ".properties");

			properties = new Properties();
			try {
				properties.load(fis);
			} catch (NullPointerException e) {
				System.out.println("File " + fileName + " Not found. Please Check");
				System.out.println(e.getMessage());
				Assert.fail("termination as file not found");

			}

		}

	}

	public String getValue(String key) throws IOException {
		String val = properties.getProperty(key);
		if (val == null) {
			return "Key " + key + " not found.";
		} else {
			return val;
		}
	}
	
	public Map<String,String> getValues(List<String> keys) throws IOException {
		Map<String,String> values = new HashMap<>();
		
		for(String key : keys) {
			values.put(key, getValue(key));
		}
		return values;
		
	}
	

	public Properties getProperties() {
		return properties;
	}

}
