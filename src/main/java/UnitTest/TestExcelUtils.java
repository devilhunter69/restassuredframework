package UnitTest;

import java.io.IOException;
import java.util.LinkedHashMap;

import org.apache.poi.EncryptedDocumentException;

import utilities.ExcelUtils;

public class TestExcelUtils {
	
	public static void main(String[] args) throws EncryptedDocumentException, IOException {
		
		ExcelUtils excelUtils = new ExcelUtils("DemoExcel.xlsx");
		
		LinkedHashMap<String,String> data = excelUtils.getExcelDataAsMap("Data");
		
		System.out.println(data);
		
	}

}
