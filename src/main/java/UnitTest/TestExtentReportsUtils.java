package UnitTest;

import com.aventstack.extentreports.ExtentTest;

import utilities.ExtentReportUtils;

public class TestExtentReportsUtils {
	
	public static void main(String[] args) {
		
		ExtentReportUtils extentReportUtils = new ExtentReportUtils("demoReport");
		ExtentTest test = extentReportUtils.getExtentReports().createTest("Test 1");
		test.pass("Step 1 passed");
		test.pass("step 2 passed");
		test.info("some info");
		test.fail("step 3 failed");
		
		ExtentTest test2 = extentReportUtils.getExtentReports().createTest("Test 2");
		test2.pass("Step 1 passed");
		test2.pass("step 2 passed");
		test2.info("some info");
		test2.fail("step 3 failed");
		
		extentReportUtils.generateExtentReport();
		
	}

}
