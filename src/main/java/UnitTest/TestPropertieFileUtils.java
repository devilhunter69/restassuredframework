package UnitTest;

import java.io.IOException;

import utilities.PropertiesFileUtils;

public class TestPropertieFileUtils {
	
	public static void main(String[] args) throws IOException {
		
		PropertiesFileUtils pf = new PropertiesFileUtils("config");
		String value = pf.getValue("frameworkName");
		
		System.out.println(value);
	}

}
